using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LissajousCurve
{
    public float x { get; protected set; }
    public float y { get; protected set; }
    public float z { get; protected set; }

    protected int tickFrequency;
    protected int tick = 0;

    // Determines movement speed (weight) of axis
    public float xSpeedModifier { get; protected set; }
    public float ySpeedModifier { get; protected set; }
    public float zSpeedModifier { get; protected set; }
    // Determines size of axis (usually kept a 1)
    protected float x_A, y_B, z_C;
    protected float x_piModifier, z_piModifier;

    public LissajousCurve(int tickFrequency, float xA, float xSpeed, float x_piMod, 
        float yB, float ySpeed, float zC, float zSpeed, float z_piMod)
    {
        this.tickFrequency = tickFrequency;
        this.xSpeedModifier = xSpeed;
        this.x_A = xA;
        this.x_piModifier = x_piMod;
        this.ySpeedModifier = ySpeed;
        this.y_B = yB;
        this.zSpeedModifier = zSpeed;
        this.z_C = zC;
        this.z_piModifier = z_piMod;
    }

    public bool Tick(float t)
    {
        if (tick % tickFrequency != 0)
        {
            // Don't do calculations
            tick++;
            return false;
        }
        tick++;

        // x = a * sin(w*t+pi)
        if (x_piModifier != 0) x = x_A * Mathf.Sin(xSpeedModifier * t + Mathf.PI / x_piModifier);
            
        // y = b * sin(t)
        y = y_B * Mathf.Sin(ySpeedModifier * t);

        // z = c * sin(q*t+delta)
        if (z_piModifier != 0 ) z = z_C * Mathf.Sin(zSpeedModifier * t + Mathf.PI / z_piModifier);
        
        return true;
    }
}
