using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CollisionDetection : MonoBehaviour
{
    public LayerMask acceptableCollisionLayers;

    public Action<GameObject> triggerCollisionEvent, collidingEvent, startCollidingEvent, stopCollidingEvent;

    protected new Collider collider;
    

    [SerializeField]protected MeshFilter meshFilter;

    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        CollisionManager.Instance.AddCollisionDetection(this);
    }

    private void OnDisable()
    {
        // Probably exiting playmode
        if (CollisionManager.Instance == null) return;

        CollisionManager.Instance.RemoveCollisionDetection(this);
    }

    public CollisionDetection CheckCollision()
    {
        return CollisionManager.Instance.CheckCollision(collider);
    }

    public CollisionDetection CollisionAllowed(Collider other)
    {
        if (other.gameObject == gameObject) return null;

        if(acceptableCollisionLayers.value == 1 << other.gameObject.layer)
        {
            return collider.bounds.Intersects(other.bounds) ? this : null;
        }

        return null;
    }

    public void TriggerCollision(GameObject go)
    {
        Debug.Log("trigger collision" + triggerCollisionEvent);
        triggerCollisionEvent?.Invoke(go);
    }

    public void StartCollision(GameObject go)
    {
        Debug.Log("start collision " + gameObject.name);
        startCollidingEvent?.Invoke(go);
    }

    public void Colliding(GameObject go)
    {
        Debug.Log("colliding");
        collidingEvent?.Invoke(go);
    }

    public void StopCollision(GameObject go)
    {
        Debug.Log("stop collision");
        stopCollidingEvent?.Invoke(go);
    }
}
