using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateLissajousUI : MonoBehaviour
{
    private static CreateLissajousUI _instance = null;
    public static CreateLissajousUI Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<CreateLissajousUI>(true);
            return _instance;
        }
    }

    [SerializeField] protected Slider tickFrequency, xA, xSpeed, xPiMod, yB, ySpeed, zA, zSpeed, zPiMod;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void OpenUI()
    {
        gameObject.SetActive(true);
        
    }

    public void CloseUI()
    {
        gameObject.SetActive(false);
    }

    public void CreateLissajousRenderer()
    {
        GameManager.Instance.CreateLissajousCurve((int)tickFrequency.value, xA.value, xSpeed.value, xPiMod.value, yB.value, ySpeed.value, zA.value, zSpeed.value, zPiMod.value);
        gameObject.SetActive(false);
    }
}
