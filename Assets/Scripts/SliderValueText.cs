using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderValueText : MonoBehaviour
{
    [SerializeField] protected Slider slider;
    protected TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    public void SliderValueChanged()
    {
        text.text = slider.value.ToString("0.00");
    }

    private void OnEnable()
    {
        SliderValueChanged();
    }
}
