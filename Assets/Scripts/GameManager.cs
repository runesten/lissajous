using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    protected Dictionary<LissajousCurve, List<LissajousRenderer>> lissajousCurves = new Dictionary<LissajousCurve, List<LissajousRenderer>>();

    [SerializeField]protected LissajousRenderer LissajousRendererPrefab;

    void Start()
    {
        //AddLissajousCurve(5, 1f, 7.5f, 1f, 1f, 2f, 1f, 1f, 1f);
        CreateLissajousCurve(5, 1f, 10f, 1f, 1f, 2f, 1f, 3f, 0.1f);
        //AddLissajousCurve(5, 1f, 1f, 1f, 1f, 2f, 1f, 3f, 0.5f);
    }

    void Update()
    {
        foreach (var lissajousCurve in lissajousCurves.Keys)
        {
            if (lissajousCurve.Tick(Time.time))
            {
                foreach (var lissajousRenderer in lissajousCurves[lissajousCurve])
                {
                    //lissajousRenderer.AddValue(lissajousCurve);
                    lissajousRenderer.AddTrailValue(lissajousCurve);
                }
            }
        }
    }

    public void CreateLissajousCurve(int tickFrequency, float xA, float xSpeed, float xPiMod, float yB, float ySpeed, float zA, float zSpeed, float zPiMod)
    {
        LissajousRenderer lr = Instantiate(LissajousRendererPrefab, Vector3.zero, Quaternion.identity).GetComponent<LissajousRenderer>();
        LissajousCurve lc = new LissajousCurve(tickFrequency, xA, xSpeed, xPiMod, yB, ySpeed, zA, zSpeed, zPiMod);
        lr.SetupLissajousRenderer(lc);
        lissajousCurves.Add(lc, new List<LissajousRenderer>() { lr });
    }

    public void RemoveLissajousRenderer(LissajousCurve lc, LissajousRenderer lr)
    {
        if (lissajousCurves[lc].Contains(lr))
        {
            lissajousCurves[lc].Remove(lr);
            if (lissajousCurves[lc].Count == 0)
            {
                lissajousCurves.Remove(lc);
            }
        }
    }
}
