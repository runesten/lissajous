using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveObject : MonoBehaviour
{
    [SerializeField]protected LayerMask deleteLayerMask;
    protected new Renderer renderer;

    void Start()
    {
        renderer = GetComponent<Renderer>();
        var cd = GetComponent<CollisionDetection>();
        cd.triggerCollisionEvent += CheckRemove;
        cd.startCollidingEvent += StartColliding;
        cd.stopCollidingEvent += StopColliding;
        
    }

    protected void CheckRemove(GameObject go)
    {
        Debug.Log("check remove " + (deleteLayerMask.value == 1 << go.layer));
        if (deleteLayerMask.value == 1 << go.layer)
        {
            Destroy(go);
        }
    }

    protected void StartColliding(GameObject go)
    {
        if (deleteLayerMask.value == 1 << go.layer)
        {
            renderer.material.shader = Shader.Find("Outlined/Custom");
            renderer.material.SetColor("_OutlineColor", Color.red);
        }
    }

    protected void StopColliding(GameObject go)
    {
        if (deleteLayerMask.value == 1 << go.layer)
        {
            renderer.material.shader = Shader.Find("Diffuse");
        }
    }
}
