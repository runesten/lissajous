using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Interactable : MonoBehaviour
{
    public Func<bool> SelectEvent = null;
    public Func<Vector3, bool> SelectedEvent = null;
    public Func<bool> DeselectedEvent = null;
    public Func<bool> StartHoverEvent = null;
    public Func<bool> HoveringEvent = null;
    public Func<bool> StopHoverEvent = null;

    public bool Select()
    {
        Debug.Log("Ray hit " + transform.name);
        if (SelectEvent != null)
            return SelectEvent.Invoke();
        return false;
    }

    public bool Selected(Vector3 translation)
    {
        //Debug.Log("Ray hit " + transform.name);
        if (SelectedEvent != null)
            return SelectedEvent.Invoke(translation);
        return false;
    }

    public bool Deselect()
    {
        //Debug.Log("Ray hit " + transform.name);
        if (DeselectedEvent != null)
            return DeselectedEvent.Invoke();
        return false;
    }

    public bool StartHover()
    {
        //Debug.Log("Start hover " + transform.name);
        if (StartHoverEvent != null)
            return StartHoverEvent.Invoke();
        return false;
    }

    public bool Hovering()
    {
        //Debug.Log("Hovering " + transform.name);
        if (HoveringEvent != null)
            return HoveringEvent.Invoke();
        return false;
    }

    public bool StopHover()
    {
        //Debug.Log("Stop hover " + transform.name);
        if(StopHoverEvent != null)
            return StopHoverEvent();
        return false;
    }


}
