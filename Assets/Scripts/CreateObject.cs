using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObject : MonoBehaviour
{
    protected new Renderer renderer;
    private void Start()
    {
        renderer = GetComponent<Renderer>();
        var interactable = GetComponent<Interactable>();
        interactable.SelectEvent += OpenObjectCreateMenu;
        interactable.StartHoverEvent += StartHover;
        interactable.StopHoverEvent+= StopHover;

    }

    protected bool OpenObjectCreateMenu()
    {
        CreateLissajousUI.Instance.OpenUI();
        return false;
    }

    protected void CreateLissajousRenderer()
    {

    }

    protected bool StartHover()
    {
        renderer.material.shader = Shader.Find("Outlined/Custom");
        renderer.material.SetColor("_OutlineColor", Color.green);
        
        return true;
    }

    protected bool StopHover()
    {
        renderer.material.shader = Shader.Find("Diffuse");
        return true;
    }
}
