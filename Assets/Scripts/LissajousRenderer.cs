using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LissajousRenderer : MonoBehaviour
{
    [SerializeField] protected TrailRenderer trailRenderer;
    [SerializeField] protected Transform xPosIndicator, yPosIndicator, zPosIndicator;
    protected LissajousCurve lissajousCurveData;

    [SerializeField] protected Interactable movementAnchor;
    [SerializeField] protected CollisionDetection collisionDetection;

    void Start()
    {
        if (trailRenderer == null) trailRenderer = GetComponent<TrailRenderer>();
        SetupMovement();
    }

    protected void SetupMovement()
    {
        movementAnchor.StartHoverEvent += () => { Debug.Log("start hover"); return true; };
        movementAnchor.StopHoverEvent += () => { Debug.Log("stop hover"); return true; };
        movementAnchor.HoveringEvent += () => { Debug.Log("hover"); return true; };
        movementAnchor.SelectEvent += () => { Debug.Log("select"); return true; };
        movementAnchor.SelectedEvent += SelectedLissajousRenderer;
        movementAnchor.DeselectedEvent += DeselectObject;
    }

    public void SetupLissajousRenderer(LissajousCurve ljCurve)
    {
        lissajousCurveData = ljCurve;
        float totalColorWeight = ljCurve.xSpeedModifier + ljCurve.ySpeedModifier + ljCurve.zSpeedModifier;

        Gradient tempGradient = new Gradient();
        GradientColorKey[] gck = new GradientColorKey[2];
        GradientAlphaKey[] gak = new GradientAlphaKey[2];
        gck[0] = new GradientColorKey(new Color(ljCurve.xSpeedModifier / totalColorWeight * 0.8f, 
            ljCurve.ySpeedModifier / totalColorWeight * 0.8f, ljCurve.zSpeedModifier / totalColorWeight * 0.8f), 0f);
        gck[1] = new GradientColorKey(new Color(ljCurve.xSpeedModifier / totalColorWeight, 
            ljCurve.ySpeedModifier / totalColorWeight, ljCurve.zSpeedModifier / totalColorWeight), 1f);
        gak[0] = new GradientAlphaKey(1, 0);
        gak[1] = new GradientAlphaKey(1, 1);

        tempGradient.SetKeys(gck, gak);
        trailRenderer.colorGradient = tempGradient;
        trailRenderer.GetComponentInChildren<MeshRenderer>().material.color = gck[0].color;
        
    }

    public void AddTrailValue(LissajousCurve ljCurve)
    {
        trailRenderer.transform.localPosition = new Vector3(ljCurve.x, ljCurve.y, ljCurve.z);
        xPosIndicator.localPosition = new Vector3(ljCurve.x, xPosIndicator.localPosition.y, xPosIndicator.localPosition.z);
        yPosIndicator.localPosition = new Vector3(yPosIndicator.localPosition.x, ljCurve.y, yPosIndicator.localPosition.z);
        zPosIndicator.localPosition = new Vector3(zPosIndicator.localPosition.x, zPosIndicator.localPosition.y, ljCurve.z);
    }

    protected CollisionDetection collidingObject;

    protected bool SelectedLissajousRenderer(Vector3 translation)
    {
        // Update positions of trailrenderer to maintain curve and not wait for it to redraw
        for (int i = 0; i < trailRenderer.positionCount; i++)
        {
            trailRenderer.SetPosition(i, trailRenderer.GetPosition(i) + translation);
        }
        transform.Translate(translation);

        var cd = collisionDetection.CheckCollision();

        if (cd != null)
        {
            if (cd != collidingObject || collidingObject == null)
            {
                cd.StartCollision(gameObject);
                if (collidingObject != null)
                {
                    collidingObject.StopCollision(gameObject);
                }
                collidingObject = cd;
            }
            cd.Colliding(gameObject);
        }
        else if (collidingObject != null)
        {
            collidingObject.StopCollision(gameObject);
            collidingObject = null;
        }
        
        return true; 
    }

    public bool DeselectObject()
    {
        var cd = collisionDetection.CheckCollision();
        if (cd != null)
        {
            cd.TriggerCollision(gameObject);
            return true;
        }
        return false;
    }

    private void OnDestroy()
    {
        // Probably exitting
        if (GameManager.Instance == null) return;

        GameManager.Instance.RemoveLissajousRenderer(lissajousCurveData, this);
    }

    [ContextMenu("Toggle Debug")]
    public void ToggleDebug()
    {
        xPosIndicator.gameObject.SetActive(!xPosIndicator.gameObject.activeInHierarchy);
        yPosIndicator.gameObject.SetActive(!yPosIndicator.gameObject.activeInHierarchy);
        zPosIndicator.gameObject.SetActive(!zPosIndicator.gameObject.activeInHierarchy);
    }
}
