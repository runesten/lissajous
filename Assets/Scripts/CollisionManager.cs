using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CollisionManager : MonoBehaviour
{
    private static CollisionManager _instance = null;
    public static CollisionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CollisionManager>();
            }

            return _instance;
        }
    }

    [SerializeField]protected List<CollisionDetection> CollisionDetections = new List<CollisionDetection>();

    void Awake()
    {
        _instance = this;
    }

    public void AddCollisionDetection(CollisionDetection cd)
    {
        if (CollisionDetections.Contains(cd))
            return;
        
        CollisionDetections.Add(cd);
    }

    public void RemoveCollisionDetection(CollisionDetection cd)
    {
        CollisionDetections.Remove(cd);
    }

    public CollisionDetection CheckCollision(Collider other)
    {
        foreach(var cd in CollisionDetections)
        {
            if (cd.CollisionAllowed(other))
            {
                return cd;
            }
        }

        return null;
    }
}
