using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInteraction : MonoBehaviour
{
    [SerializeField]protected float MoveSpeed, horizontalRotationSpeed, verticalRotationSpeed;

    protected Interactable lastInteractableActive;
    protected Interactable selectedInteractable;

    protected float selectedDistance;

    void Update()
    {
        Movement();
        Rotation();
        Aiming();

        if (selectedInteractable != null)
        {   
            SelectedInteractableActive();    
        }
    }

    protected void Movement()
    {
        float zAxis = 0;
        if (Input.GetKey(KeyCode.Q)) zAxis = -1;
        else if (Input.GetKey(KeyCode.E)) zAxis = 1;

        Vector3 translation = new Vector3(Input.GetAxis("Horizontal"), zAxis, Input.GetAxis("Vertical"));
        translation *= MoveSpeed * Time.deltaTime;

        transform.Translate(translation);
    }
    float rotationY = 0;
    protected void Rotation()
    {
        if (Input.GetMouseButton(1))
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * horizontalRotationSpeed;

            rotationY += Input.GetAxis("Mouse Y") * verticalRotationSpeed;

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
    }

    protected void Aiming()
    {
        if (selectedInteractable != null) 
        { 
            if (Input.GetKeyDown(KeyCode.Space))
            {
                selectedInteractable.Deselect();
                selectedInteractable = null;
            }
            return; 
        }

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            Interactable interactable;
            if (hit.transform.TryGetComponent<Interactable>(out interactable))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (interactable.Select())
                    {
                        selectedInteractable = interactable;
                        selectedDistance = Vector3.Distance(transform.position, selectedInteractable.transform.position);
                    }
                }

                if (interactable != lastInteractableActive)
                {
                    interactable.StartHover();
                    if (lastInteractableActive != null) { lastInteractableActive.StopHover(); }
                    lastInteractableActive = interactable;
                }
                
                lastInteractableActive.Hovering();
            }
            
            // if (interactable != null)
            // {
                
            // }
        }
        else
        {
            if (lastInteractableActive != null)
            {
                lastInteractableActive.StopHover();
                lastInteractableActive = null;
            }
        }
    }

    protected void SelectedInteractableActive()
    {
        var pos = transform.position + transform.forward * selectedDistance;
        var relativPos = pos - selectedInteractable.transform.position;
        selectedInteractable.SelectedEvent?.Invoke(relativPos);
    }
}
